# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

import pytest

from app.application import get_application


@pytest.fixture
def app():
    yield get_application()
