# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

import pytest
from uuid import UUID

from app.domain import (
    RebusPost,
    RebusPostLocation,
    RebusPostQuestion,
    RebusRace,
    Team,
    TeamMember,
)
from app.exceptions import (
    RebusRaceNotFoundError,
    RebusPostNotFoundError,
    RebusPostQuestionNotFoundError,
    TeamNotFoundError,
    TeamMemberNotFoundError,
)


def test_rebus_races(app):
    assert list(app.get_rebus_races()) == []
    with pytest.raises(RebusRaceNotFoundError):
        app.get_rebus_race(UUID("1236f8cf-0ada-499d-93ed-ce5d771af8c4"))
    rebus_race = app.create_rebus_race("testrace")
    assert isinstance(rebus_race, RebusRace)
    rebus_race = app.get_rebus_race(rebus_race.id)
    assert rebus_race.title == "testrace"

    rebus_race_updated = app.update_rebus_race_title(rebus_race.id, "testtitle")
    assert rebus_race_updated.id == rebus_race.id
    assert rebus_race_updated.title == "testtitle"


def test_rebus_post(app):
    rebus_race = app.create_rebus_race("testrace")
    assert rebus_race.rebus_posts == []
    rebus_post1 = RebusPost(title="first post")
    with pytest.raises(RebusRaceNotFoundError):
        app.add_rebus_post(UUID("1236f8cf-0ada-499d-93ed-ce5d771af8c4"), rebus_post1)
    rebus_post_added = app.add_rebus_post(rebus_race.id, rebus_post1)
    assert rebus_post_added == rebus_post1
    assert app.get_rebus_race(rebus_race.id).rebus_posts == [rebus_post1]

    question1 = RebusPostQuestion(question="some question")
    with pytest.raises(RebusRaceNotFoundError):
        app.add_rebus_post_question(
            UUID("1236f8cf-0ada-499d-93ed-ce5d771af8c4"), rebus_post1.id, question1
        )
    with pytest.raises(RebusPostNotFoundError):
        app.add_rebus_post_question(
            rebus_race.id, UUID("1236f8cf-0ada-499d-93ed-ce5d771af8c4"), question1
        )
    rebus_post_added = app.add_rebus_post_question(
        rebus_race.id, rebus_post1.id, question1
    )
    assert rebus_post_added.id == rebus_post1.id
    assert rebus_post_added.questions == [question1]

    rebus_race_question_removed = app.remove_rebus_post_question(
        rebus_race.id, rebus_post1.id, question1.id
    )
    assert rebus_race_question_removed.rebus_posts[0].questions == []
    rebus_race_post_removed = app.remove_rebus_post(rebus_race.id, rebus_post1.id)
    assert rebus_race_post_removed.rebus_posts == []


def test_rebus_post_unlock():
    rebus_post1 = RebusPost(title="post 1")
    assert not rebus_post1.unlock_post(unlock_code="123")
    assert rebus_post1.unlock_post(unlock_code=rebus_post1.unlock_code)
    location1 = RebusPostLocation(latitude=59.054903, longitude=10.533111, leeway=50)
    rebus_post2 = RebusPost(title="post 2", location=location1)
    assert not rebus_post2.unlock_post(latitude=59.056672, longitude=10.534286)
    assert rebus_post2.unlock_post(latitude=59.054976, longitude=10.533235)
    location2 = RebusPostLocation(latitude=59.054903, longitude=10.533111, leeway=500)
    rebus_post3 = RebusPost(title="post 2", location=location2)
    assert rebus_post3.unlock_post(latitude=59.056672, longitude=10.534286)


def test_team(app):
    rebus_race = app.create_rebus_race("testrace")
    assert rebus_race.teams == []
    team = Team("test team 1")

    with pytest.raises(RebusRaceNotFoundError):
        app.add_team(UUID("1236f8cf-0ada-499d-93ed-ce5d771af8c4"), team)
    team_added = app.add_team(rebus_race.id, team)
    assert team == team_added
    assert app.get_rebus_race(rebus_race.id).teams == [team]

    with pytest.raises(RebusRaceNotFoundError):
        app.update_team_name(
            "1236f8cf-0ada-499d-93ed-ce5d771af8c4", team.id, "test team new name"
        )
    with pytest.raises(TeamNotFoundError):
        app.update_team_name(
            rebus_race.id, "1236f8cf-0ada-499d-93ed-ce5d771af8c4", "test team new name"
        )
    team_new_name = app.update_team_name(rebus_race.id, team.id, "test team new name")
    assert team.id == team_new_name.id
    assert team_new_name.name == "test team new name"

    member1 = TeamMember("member 1")
    member2 = TeamMember("member 2")
    with pytest.raises(TeamNotFoundError):
        app.add_team_members(
            rebus_race.id, "1236f8cf-0ada-499d-93ed-ce5d771af8c4", [member1, member2]
        )
    team_members = app.add_team_members(rebus_race.id, team.id, [member1, member2])
    assert team_members.id == team.id
    assert team_members.members == [member1, member2]
    # team_member_removed = app.remove_team_member(rebus_race.id, team.id, "1236f8cf-0ada-499d-93ed-ce5d771af8c4")
    with pytest.raises(TeamMemberNotFoundError):
        app.remove_team_member(
            rebus_race.id, team.id, "1236f8cf-0ada-499d-93ed-ce5d771af8c4"
        )

    team_member_removed = app.remove_team_member(rebus_race.id, team.id, member2.id)
    assert team_member_removed.members == [member1]

    rebus_race_team_removed = app.remove_team(rebus_race.id, team.id)
    assert rebus_race_team_removed.teams == []
