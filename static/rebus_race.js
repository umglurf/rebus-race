// SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
//
// SPDX-License-Identifier: GPL-3.0-only

var graph = graphql("/graphql", {
  asJSON: true,
});

function addAlert(obj, msg, alertType) {
  let alertElement = document.createElement("div")
  alertElement.className = "alert alert-" + alertType + " alert-dismissible fade show";
  alertElement.setAttribute("role", "alert");
  alertElement.textContent = msg;
  let alertButton = document.createElement("button");
  alertButton.type = "button";
  alertButton.className = "btn-close";
  alertButton.setAttribute("data-bs-dismiss", "alert")
  alertButton.ariaLabel = "Close";
  alertElement.append(alertButton);
  obj.after(alertElement);
  setTimeout(() => {
    alertElement.parentElement.removeChild(alertElement);
  }, 5000);
}

const changeRebusLockedQuery = graph(`
mutation changeLocked($input: ChangeRebusRaceLockedInput!) {
  changeRebusRaceLocked(input: $input) {
    ...on ChangeRebusRaceLockedPayload {
      locked
    }
    ...on RebusRaceNotFound {
      message
    }
  }
}
`);

const removeRebusPostQuery = graph(`
mutation remove($input: RemoveRebusPostInput!) {
  removeRebusPost(input: $input) {
    ...on RemoveRebusPostPayload {
      rebusRace {
        Id
        title
      }
    }
    ...on RebusRaceNotFound {
      message
    }
    ...on RebusRaceLocked {
      message
    }
    ...on RebusPostNotFound {
      message
    }
  }
}
`);

const removeRebusPostQuestionQuery = graph(`
mutation remove($input: RemoveRebusPostQuestionInput!) {
  removeRebusPostQuestion(input: $input) {
    ...on RemoveRebusPostQuestionPayload {
      rebusPost {
        id
        title
      }
    }
    ...on RebusRaceNotFound {
      message
    }
    ...on RebusRaceLocked {
      message
    }
    ...on RebusPostNotFound {
      message
    }
    ...on RebusPostQuestionNotFound {
      message
    }
  }
}
`);

const removeTeamQuery = graph(`
mutation remove($input: RemoveTeamInput!) {
  removeTeam(input: $input) {
    ...on RemoveTeamPayload {
      rebusRace {
        Id
        title
      }
    }
    ...on RebusRaceNotFound {
      message
    }
    ...on RebusRaceLocked {
      message
    }
    ...on TeamNotFound {
      message
    }
  }
}
`);

const removeTeamMemberQuery = graph(`
mutation remove($input: RemoveTeamMemberInput!) {
  removeTeamMember(input: $input) {
    ...on RemoveTeamMemberPayload {
      team {
        id
        name
      }
    }
    ...on RebusRaceNotFound {
      message
    }
    ...on RebusRaceLocked {
      message
    }
    ...on TeamNotFound {
      message
    }
    ...on TeamMemberNotFound {
      message
    }
  }
}
`);

const updateRebusRaceTitleQuery = graph(`
mutation update($input: UpdateRebusRaceTitleInput!) {
  updateRebusRaceTitle(input: $input) {
    ... on UpdateRebusRaceTitlePayload {
      rebusRace {
        Id
        title
      }
    }
    ... on RebusRaceNotFound {
      message
    }
    ...on RebusRaceLocked {
      message
    }
  }
}
`);

const updateRebusPostTitleQuery = graph(`
mutation update($input: UpdateRebusPostTitleInput!) {
  updateRebusPostTitle(input: $input) {
    ... on UpdateRebusPostTitlePayload {
      rebusPost {
        id
        title
      }
    }
    ... on RebusRaceNotFound {
      message
    }
    ...on RebusRaceLocked {
      message
    }
    ... on RebusPostNotFound {
      message
    }
  }
}
`);

const updateRebusPostQuestionTeamAnswerQuery = graph(`
mutation update($input: UpdateRebusPostQuestionTeamAnswerInput!) {
  updateRebusPostQuestionTeamAnswer(input: $input) {
    ... on UpdateRebusPostQuestionTeamAnswerPayload {
      rebusPostQuestion {
        id
      }
    }
    ... on RebusRaceNotFound {
      message
    }
    ...on RebusRaceLocked {
      message
    }
    ... on RebusPostNotFound {
      message
    }
    ... on RebusPostQuestionNotFound {
      message
    }
    ... on TeamNotFound {
      message
    }
  }
}
`);

const updateRebusPostQuestionTeamAnswerScoreQuery = graph(`
mutation update($input: UpdateRebusPostQuestionTeamAnswerScoreInput!) {
  updateRebusPostQuestionTeamAnswerScore(input: $input) {
    ... on UpdateRebusPostQuestionTeamAnswerScorePayload {
      rebusPostQuestion {
        id
      }
    }
    ... on RebusRaceNotFound {
      message
    }
    ...on RebusRaceLocked {
      message
    }
    ... on RebusPostNotFound {
      message
    }
    ... on RebusPostQuestionNotFound {
      message
    }
    ... on TeamNotFound {
      message
    }
  }
}
`);

const updateTeamNameQuery = graph(`
mutation remove($input: UpdateTeamNameInput!) {
  updateTeamName(input: $input) {
    ...on UpdateTeamNamePayload {
      team {
        id
        name
      }
    }
    ...on RebusRaceNotFound {
      message
    }
    ...on RebusRaceLocked {
      message
    }
    ...on TeamNotFound {
      message
    }
  }
}
`);

function lockRebusRace() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  changeRebusLockedQuery({
    input: {
      rebusRaceId: rebusRaceId,
      locked: true
    }
  }).then(response => {
    let parent = this.parentElement;
    parent.removeChild(this);
    let unlockElement = document.createElement("button")
    unlockElement.type = "button";
    unlockElement.id = "unlockRebusRace";
    unlockElement.className = "btn btn-primary btn-sm";
    unlockElement.textContent = "Unlock rebus race";
    unlockElement.addEventListener("click", unlockRebusRace);
    parent.append(unlockElement);

  }).catch(error => {
    addAlert(this.parentElement, "Error locking rebus race", "danger");
  });
};

function unlockRebusRace() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  changeRebusLockedQuery({
    input: {
      rebusRaceId: rebusRaceId,
      locked: false
    }
  }).then(response => {
    let parent = this.parentElement;
    parent.removeChild(this);
    let unlockElement = document.createElement("button")
    unlockElement.type = "button";
    unlockElement.id = "lockRebusRace";
    unlockElement.className = "btn btn-primary btn-sm";
    unlockElement.textContent = "Lck rebus race";
    unlockElement.addEventListener("click", lockRebusRace);
    parent.append(unlockElement);

  }).catch(error => {
    addAlert(this.parentElement, "Error unlocking rebus race", "danger");
  });
};

function changeRebusRaceTitle() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let title = this.parentElement.querySelector("input").value;
  updateRebusRaceTitleQuery({
    input: {
      id: rebusRaceId,
      title: title
    }
  }).then(response => {
    document.querySelector("h1").textContent = title;
    addAlert(this.parentElement, "Title changed", "info");
  }).catch(error => {
    addAlert(this.parentElement, "Error changing title", "danger");
  });
};

function changeRebusPostTitle() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let rebusPostId = document.querySelector("#rebusPostId").dataset.id;
  let title = this.parentElement.querySelector("input").value;
  updateRebusPostTitleQuery({
    input: {
      rebusRaceId: rebusRaceId,
      rebusPostId: rebusPostId,
      title: title
    }
  }).then(response => {
    document.querySelector("h1").textContent = title;
    addAlert(this.parentElement, "Title changed", "info");
  }).catch(error => {
    addAlert(this.parentElement, "Error changing title", "danger");
  });
};

function changeTeamName() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let teamId = document.querySelector("#teamId").dataset.id;
  let name = this.parentElement.querySelector("input").value;
  updateTeamNameQuery({
    input: {
      rebusRaceId: rebusRaceId,
      teamId: teamId,
      name: name
    }
  }).then(response => {
    document.querySelector("h1").textContent = "Team " + name;
    addAlert(this.parentElement, "Title changed", "info");
  }).catch(error => {
    console.log(error);
    addAlert(this.parentElement, "Error changing title", "danger");
  });
};

function removeRebusPost() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let rebusPostId = this.dataset.rebusPostId;
  let listElement = document.querySelector("button[data-rebus-post-id='" + rebusPostId + "']").parentElement.parentElement.parentElement.parentElement;
  removeRebusPostQuery({
    input: {
      rebusRaceId: rebusRaceId,
      rebusPostId: rebusPostId
    }
  }).then(response => {
    this.parentElement.querySelector("button.close").click();
    addAlert(listElement.parentElement, "Rebus post removed", "info");
    listElement.parentElement.removeChild(listElement);
  }).catch(error => {
    this.parentElement.querySelector("button.close").click();
    addAlert(listElement.parentElement, "Error removing rebus post", "danger");
  });
};

function removePostQuestion() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let rebusPostId = document.querySelector("#rebusPostId").dataset.id;
  let questionId = this.dataset.questionId;
  let listElement = this.parentElement;
  removeRebusPostQuestionQuery({
    input: {
      rebusRaceId: rebusRaceId,
      rebusPostId: rebusPostId,
      rebusPostQuestionId: questionId
    }
  }).then(response => {
    addAlert(listElement.parentElement, "Rebus post question removed", "info");
    listElement.parentElement.removeChild(listElement);
  }).catch(error => {
    addAlert(listElement.parentElement, "Error removing rebus post question", "danger");
  });
};

function removeTeam() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let teamId = this.dataset.teamId;
  let listElement = document.querySelector("button[data-team-id='" + teamId + "']").parentElement.parentElement.parentElement.parentElement;
  removeTeamQuery({
    input: {
      rebusRaceId: rebusRaceId,
      teamId: teamId
    }
  }).then(response => {
    this.parentElement.querySelector("button.close").click();
    addAlert(listElement.parentElement, "Team removed", "info");
    listElement.parentElement.removeChild(listElement);
  }).catch(error => {
    this.parentElement.querySelector("button.close").click();
    addAlert(listElement.parentElement, "Error removing team", "danger");
  });
};

function removeTeamMember() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let teamId = this.dataset.teamId;
  let teamMemberId = this.dataset.teamMemberId;
  let listElement = this.parentElement;
  removeTeamMemberQuery({
    input: {
      rebusRaceId: rebusRaceId,
      teamId: teamId,
      teamMemberId: teamMemberId
    }
  }).then(response => {
    addAlert(listElement.parentElement, "Team member removed", "info");
    listElement.parentElement.removeChild(listElement);
  }).catch(error => {
    addAlert(listElement.parentElement, "Error removing team member", "danger");
  });
};

const GPSoptions = {
  enableHighAccuracy: true,
  timeout: 10000,
  maximumAge: 0,
};

function updateRebusPostQuestionTeamAnswer() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let teamId = document.querySelector("#teamId").dataset.id;
  let rebusPostId = this.dataset.rebusPostId;
  let questionId = this.dataset.questionId;
  let answer = this.parentElement.querySelector("input").value;
  updateRebusPostQuestionTeamAnswerQuery({
    input: {
      rebusRaceId: rebusRaceId,
      rebusPostId: rebusPostId,
      rebusPostQuestionId: questionId,
      teamId: teamId,
      answer: answer
    }
  }).then(response => {
    addAlert(this.parentElement, "Answer saved", "info");
  }).catch(error => {
    addAlert(this.parentElement, "Error saving answer", "danger");
  });
};

function updateRebusPostQuestionTeamAnswerScore() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let rebusPostId = document.querySelector("#rebusPostId").dataset.id;
  let questionId = this.dataset.questionId;
  let teamId = this.dataset.teamId;
  let score = parseFloat(this.value);
  updateRebusPostQuestionTeamAnswerScoreQuery({
    input: {
      rebusRaceId: rebusRaceId,
      rebusPostId: rebusPostId,
      rebusPostQuestionId: questionId,
      teamId: teamId,
      score: score
    }
  }).then(response => {
    addAlert(this.parentElement, "Score saved", "info");
  }).catch(error => {
    addAlert(this.parentElement, "Error saving score", "danger");
  });
};

function unlockGPS() {
  let rebusRaceId = document.querySelector("#rebusRaceId").dataset.id;
  let teamId = document.querySelector("#teamId").dataset.id;
  console.log("unlocking");
  navigator.geolocation.getCurrentPosition(pos => {
    this.parentElement.querySelector("input[name='latitude']").value = pos.coords.latitude;
    this.parentElement.querySelector("input[name='longitude']").value = pos.coords.longitude;
    this.parentElement.submit();
  }, err => {
    addAlert(this.parentElement.parentElement, "Error getting GPS coordinates: " + error.message);
  }, GPSoptions);
};

document.addEventListener("DOMContentLoaded", function(event) {
  document.querySelectorAll("#changeRebusRaceTitle").forEach(el => {
    el.addEventListener("click", changeRebusRaceTitle)
  });

  document.querySelectorAll("#changeRebusPostTitle").forEach(el => {
    el.addEventListener("click", changeRebusPostTitle)
  });

  document.querySelectorAll("#changeTeamName").forEach(el => {
    el.addEventListener("click", changeTeamName)
  });

  document.querySelectorAll("#removeRebusPostModal").forEach(el => {
    el.addEventListener("show.bs.modal", event => {
      let rebusPostId = event.relatedTarget.dataset.rebusPostId;
      let rebusPostTitle = event.relatedTarget.dataset.rebusPostTitle;
      event.target.querySelector(".modal-body").innerText = "Are you sure you want to remove the post " + rebusPostTitle + "?";
      event.target.querySelector("#confirmRemoveRebusPost").dataset.rebusPostId = rebusPostId;
      event.target.querySelector("#confirmRemoveRebusPost").dataset.rebusPostTitle = rebusPostTitle;
    });
  });
  document.querySelectorAll("#confirmRemoveRebusPost").forEach(el => {
    el.addEventListener("click", removeRebusPost)
  });

  document.querySelectorAll("button.removePostQuestion").forEach(el => {
    el.addEventListener("click", removePostQuestion);
  });

  document.querySelectorAll("#removeTeamModal").forEach(el => {
    el.addEventListener("show.bs.modal", event => {
      let teamId = event.relatedTarget.dataset.teamId;
      let teamName = event.relatedTarget.dataset.teamName;
      event.target.querySelector(".modal-body").innerText = "Are you sure you want to remove the team " + teamName + "?";
      event.target.querySelector("#confirmRemoveTeam").dataset.teamId = teamId;
      event.target.querySelector("#confirmRemoveTeam").dataset.teamName = teamName;
    });
  });
  document.querySelectorAll("#confirmRemoveTeam").forEach(el => {
    el.addEventListener("click", removeTeam)
  });

  document.querySelectorAll("button.removeTeamMember").forEach(el => {
    el.addEventListener("click", removeTeamMember);
  });

  document.querySelectorAll("button.saveAnswer").forEach(el => {
    el.addEventListener("click", updateRebusPostQuestionTeamAnswer);
  });

  document.querySelectorAll("input.answerScore").forEach(el => {
    el.addEventListener("change", updateRebusPostQuestionTeamAnswerScore);
  });

  document.querySelectorAll("button.unlockGPS").forEach(el => {
    el.addEventListener("click", unlockGPS);
  });

  document.querySelectorAll("button.lockRebusRace").forEach(el => {
    el.addEventListener("click", lockRebusRace);
  });

  document.querySelectorAll("button.unlockRebusRace").forEach(el => {
    el.addEventListener("click", unlockRebusRace);
  });
});
