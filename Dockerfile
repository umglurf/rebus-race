# SPDX-FileCopyrightText: 2020 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

FROM python:3.11.2-slim-bullseye AS base

RUN export DEBIAN_FRONTEND noninteractive \
    && apt-get update \
    && apt-get install -y dumb-init libpq5 \
    && apt-get clean -y \
    && find /var/lib/apt/lists -delete

RUN groupadd -g 1000 app && useradd -u 1000 -g app -m app

RUN pip3 install pipenv==2023.2.18

WORKDIR /app

FROM base as build

env DEBIAN_FRONTEND noninteractive
RUN apt-get update \
    && apt-get install -y build-essential dumb-init libpq-dev

USER 1000

COPY Pipfile Pipfile.lock /app

RUN pipenv install --deploy

FROM base as prod

USER 1000

COPY --from=build /app /app
COPY --from=build /home/app /home/app
COPY app/ /app/app/
COPY static/ /app/static/
COPY templates/ /app/templates/

ENV LC_ALL C.UTF-8
ENV PYTHONUNBUFFERED 1

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["pipenv", "run", "uvicorn", "--host", "::", "--proxy-headers", "app.main:app"]
