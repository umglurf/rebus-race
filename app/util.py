# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from enum import Enum
from fastapi import Request
from pydantic import BaseModel
from typing import List


class FlashMessageCategory(str, Enum):
    info = "info"
    success = "success"
    warning = "warning"
    error = "error"


class FlashMessage(BaseModel):
    category: FlashMessageCategory
    message: str


def flash(request: Request, message: FlashMessage) -> None:
    if "_messages" not in request.session:
        request.session["_messages"] = []
        request.session["_messages"].append(message.dict())


def get_flashed_messages(request: Request) -> List[FlashMessage]:
    print(request.session)
    return request.session.pop("_messages") if "_messages" in request.session else []
