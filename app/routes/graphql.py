# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from fastapi.exceptions import HTTPException
import strawberry
from strawberry.asgi import GraphQL
from strawberry.permission import BasePermission
from strawberry.types import Info
from strawberry.union import union
from typing import Any, List, Optional, Union
from uuid import UUID

from app.routes.admin import rebus_race


from ..application import get_application
from ..dependencies import is_admin, is_valid_user
from ..domain import RebusRace, RebusPost, RebusPostQuestion, Team, TeamMember
from ..exceptions import (
    RebusRaceNotFoundError,
    RebusRaceLockedError,
    RebusPostNotFoundError,
    RebusPostQuestionNotFoundError,
    TeamNotFoundError,
    TeamMemberNotFoundError,
)

app = get_application()


class IsAdmin(BasePermission):
    message = "User is not admin"

    async def has_permission(self, source: Any, info: Info, **kwargs) -> bool:
        try:
            await is_admin(info.context["request"])
            return True
        except Exception:
            return False


class IsAdminOrUser(BasePermission):
    message = "User is not authorized"

    async def has_permission(
        self, source: Any, info: Info, input: Any, **kwargs
    ) -> bool:
        try:
            await is_admin(request=info.context["request"])
            return True
        except HTTPException:
            pass
        except Exception:
            return False
        if not "rebusRaceId" in input:
            return False
        if not "teamId" in input:
            return False
        try:
            await is_valid_user(
                request=info.context["request"],
                rebus_race_id=UUID(input["rebusRaceId"]),
                team_id=UUID(input["teamId"]),
            )
            return True
        except Exception:
            return False


@strawberry.type
class Query:
    @strawberry.field(permission_classes=[IsAdmin])
    def rebus_races(
        self, id: Optional[strawberry.ID] = strawberry.UNSET
    ) -> List[RebusRace]:
        return [
            rebus_race
            for rebus_race in app.get_rebus_races()
            if id is strawberry.UNSET or UUID(rebus_race.id) == id
        ]


@strawberry.input
class CreateRebusRaceInput:
    title: str


@strawberry.type
class CreateRebusRacePayload:
    rebus_race: RebusRace


@strawberry.input
class UpdateRebusRaceTitleInput:
    id: strawberry.ID
    title: str


@strawberry.type
class UpdateRebusRaceTitlePayload:
    rebus_race: RebusRace


@strawberry.type
class RebusRaceNotFound:
    message: str


@strawberry.type
class RebusRaceLocked:
    message: str


UpdateRebusRaceTitleOutput = strawberry.union(
    "UpdateRebusRaceTitleOutput",
    types=[UpdateRebusRaceTitlePayload, RebusRaceNotFound, RebusRaceLocked],
)


@strawberry.input
class TeamMemberInput:
    name: str


@strawberry.input
class CreateTeamInput:
    rebus_race_id: strawberry.ID
    name: str
    members: Optional[List[TeamMemberInput]]


@strawberry.type
class CreateTeamPayload:
    team: Team


CreateTeamOutput = strawberry.union(
    "CreateTeamOutput", types=[CreateTeamPayload, RebusRaceNotFound, RebusRaceLocked]
)


@strawberry.input
class UpdateTeamNameInput:
    rebus_race_id: strawberry.ID
    team_id: strawberry.ID
    name: str


@strawberry.type
class UpdateTeamNamePayload:
    team: Team


@strawberry.type
class TeamNotFound:
    message: str


UpdateTeamNameOutput = strawberry.union(
    "UpdateTeamNameOutput",
    types=[UpdateTeamNamePayload, RebusRaceNotFound, RebusRaceLocked, TeamNotFound],
)


@strawberry.input
class AddTeamMembersInput:
    rebus_race_id: strawberry.ID
    team_id: strawberry.ID
    members: List[TeamMemberInput]


@strawberry.type
class AddTeamMembersPayload:
    team: Team


AddTeamMembersOutput = strawberry.union(
    "AddTeamMembersOutput",
    types=[AddTeamMembersPayload, RebusRaceNotFound, RebusRaceLocked, TeamNotFound],
)


@strawberry.input
class RemoveTeamMemberInput:
    rebus_race_id: strawberry.ID
    team_id: strawberry.ID
    team_member_id: strawberry.ID


@strawberry.type
class TeamMemberNotFound:
    message: str


@strawberry.type
class RemoveTeamMemberPayload:
    team: Team


RemoveTeamMemberOutput = strawberry.union(
    "RemoveTeamMemberOutput",
    types=[
        RemoveTeamMemberPayload,
        RebusRaceNotFound,
        RebusRaceLocked,
        TeamNotFound,
        TeamMemberNotFound,
    ],
)


@strawberry.input
class RemoveTeamInput:
    rebus_race_id: strawberry.ID
    team_id: strawberry.ID


@strawberry.type
class RemoveTeamPayload:
    rebus_race: RebusRace


RemoveTeamOutput = strawberry.union(
    "RemoveTeamOutput",
    types=[
        RemoveTeamPayload,
        RebusRaceNotFound,
        RebusRaceLocked,
        TeamNotFound,
    ],
)


@strawberry.type
class RebusPostNotFound:
    message: str


@strawberry.input
class RemoveRebusPostInput:
    rebus_race_id: strawberry.ID
    rebus_post_id: strawberry.ID


@strawberry.type
class RemoveRebusPostPayload:
    rebus_race: RebusRace


RemoveRebusPostOutput = strawberry.union(
    "RemoveRebusPostOutput",
    types=[
        RemoveRebusPostPayload,
        RebusRaceNotFound,
        RebusRaceLocked,
        RebusPostNotFound,
    ],
)


@strawberry.input
class UpdateRebusPostTitleInput:
    rebus_race_id: strawberry.ID
    rebus_post_id: strawberry.ID
    title: str


@strawberry.type
class UpdateRebusPostTitlePayload:
    rebus_post: RebusPost


UpdateRebusPostTitleOutput = strawberry.union(
    "UpdateRebusPostTitleOutput",
    types=[
        UpdateRebusPostTitlePayload,
        RebusRaceNotFound,
        RebusRaceLocked,
        RebusPostNotFound,
    ],
)


@strawberry.type
class RebusPostQuestionNotFound:
    message: str


@strawberry.input
class RemoveRebusPostQuestionInput:
    rebus_race_id: strawberry.ID
    rebus_post_id: strawberry.ID
    rebus_post_question_id: strawberry.ID


@strawberry.type
class RemoveRebusPostQuestionPayload:
    rebus_post: RebusPost


RemoveRebusPostQuestionOutput = strawberry.union(
    "RemoveRebusPostQuestionOutput",
    types=[
        RemoveRebusPostQuestionPayload,
        RebusRaceNotFound,
        RebusRaceLocked,
        RebusPostNotFound,
        RebusPostQuestionNotFound,
    ],
)


@strawberry.input
class UpdateRebusPostQuestionTeamAnswerInput:
    rebus_race_id: strawberry.ID
    rebus_post_id: strawberry.ID
    rebus_post_question_id: strawberry.ID
    team_id: strawberry.ID
    answer: str


@strawberry.type
class UpdateRebusPostQuestionTeamAnswerPayload:
    rebus_post_question: RebusPostQuestion


UpdateRebusPostQuestionTeamAnswerOutput = strawberry.union(
    "UpdateRebusPostQuestionTeamAnswerOutput",
    types=[
        UpdateRebusPostQuestionTeamAnswerPayload,
        RebusRaceNotFound,
        RebusRaceLocked,
        RebusPostNotFound,
        RebusPostQuestionNotFound,
        TeamNotFound,
    ],
)


@strawberry.input
class UpdateRebusPostQuestionTeamAnswerScoreInput:
    rebus_race_id: strawberry.ID
    rebus_post_id: strawberry.ID
    rebus_post_question_id: strawberry.ID
    team_id: strawberry.ID
    score: float


@strawberry.type
class UpdateRebusPostQuestionTeamAnswerScorePayload:
    rebus_post_question: RebusPostQuestion


UpdateRebusPostQuestionTeamAnswerScoreOutput = strawberry.union(
    "UpdateRebusPostQuestionTeamAnswerScoreOutput",
    types=[
        UpdateRebusPostQuestionTeamAnswerScorePayload,
        RebusRaceNotFound,
        RebusRaceLocked,
        RebusPostNotFound,
        RebusPostQuestionNotFound,
        TeamNotFound,
    ],
)


@strawberry.input
class ChangeRebusRaceLockedInput:
    rebus_race_id: strawberry.ID
    locked: bool


@strawberry.type
class ChangeRebusRaceLockedPayload:
    locked: bool


ChangeRebusRaceLockedOutput = strawberry.union(
    "ChangeRebusRaceLockedOutput",
    types=[ChangeRebusRaceLockedPayload, RebusRaceNotFound],
)


@strawberry.type
class Mutation:
    @strawberry.mutation(permission_classes=[IsAdmin])
    def add_team_members(self, input: AddTeamMembersInput) -> AddTeamMembersOutput:
        try:
            members = []
            for member in input.members:
                members.append(TeamMember(name=member.name))
            team = app.add_team_members(
                rebus_race_id=UUID(input.rebus_race_id),
                team_id=UUID(input.team_id),
                members=members,
            )
            return AddTeamMembersPayload(team=team)
        except RebusRaceNotFoundError as e:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))
        except TeamNotFoundError as e:
            return TeamNotFound(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdmin])
    def change_rebus_race_locked(
        self, input: ChangeRebusRaceLockedInput
    ) -> ChangeRebusRaceLockedOutput:
        try:
            if input.locked:
                app.lock_rebus_race(rebus_race_id=UUID(input.rebus_race_id))
            else:
                app.unlock_rebus_race(rebus_race_id=UUID(input.rebus_race_id))
            return ChangeRebusRaceLockedPayload(locked=input.locked)
        except RebusRaceNotFoundError as e:
            return RebusRaceNotFound(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdmin])
    def create_rebus_race(self, input: CreateRebusRaceInput) -> CreateRebusRacePayload:
        rebus_race = app.create_rebus_race(input.title)
        return CreateRebusRacePayload(rebus_race=rebus_race)

    @strawberry.mutation(permission_classes=[IsAdmin])
    def create_team(self, input: CreateTeamInput) -> CreateTeamOutput:
        team = Team(name=input.name)
        if input.members is not None:
            for member in input.members:
                team_member = TeamMember(name=member.name)
                team.add_team_member(team_member)
        try:
            app.add_team(rebus_race_id=UUID(input.rebus_race_id), team=team)
            return CreateTeamPayload(team=team)
        except RebusRaceNotFoundError as e:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdmin])
    def remove_rebus_post(self, input: RemoveRebusPostInput) -> RemoveRebusPostOutput:
        try:
            rebus_race = app.remove_rebus_post(
                rebus_race_id=UUID(input.rebus_race_id),
                rebus_post_id=UUID(input.rebus_post_id),
            )
            return RemoveRebusPostPayload(rebus_race=rebus_race)
        except RebusRaceNotFoundError:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))
        except RebusPostNotFoundError as e:
            return RebusPostNotFound(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdmin])
    def remove_rebus_post_question(
        self, input: RemoveRebusPostQuestionInput
    ) -> RemoveRebusPostQuestionOutput:
        try:
            rebus_post = app.remove_rebus_post_question(
                rebus_race_id=UUID(input.rebus_race_id),
                rebus_post_id=UUID(input.rebus_post_id),
                question_id=UUID(input.rebus_post_question_id),
            )
            return RemoveRebusPostQuestionPayload(rebus_post=rebus_post)
        except RebusRaceNotFoundError:
            return RebusRaceNotFound(message=str(e))
        except RebusPostNotFoundError as e:
            return RebusPostNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))
        except RebusPostQuestionNotFoundError as e:
            return RebusPostQuestionNotFound(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdmin])
    def remove_team(self, input: RemoveTeamInput) -> RemoveTeamOutput:
        try:
            rebus_race = app.remove_team(
                rebus_race_id=UUID(input.rebus_race_id),
                team_id=UUID(input.team_id),
            )
            return RemoveTeamPayload(rebus_race=rebus_race)
        except RebusRaceNotFoundError:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))
        except TeamNotFoundError as e:
            return TeamNotFound(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdmin])
    def remove_team_member(
        self, input: RemoveTeamMemberInput
    ) -> RemoveTeamMemberOutput:
        try:
            team = app.remove_team_member(
                rebus_race_id=UUID(input.rebus_race_id),
                team_id=UUID(input.team_id),
                team_member_id=UUID(input.team_member_id),
            )
            return RemoveTeamMemberPayload(team=team)
        except RebusRaceNotFoundError:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))
        except TeamNotFoundError as e:
            return TeamNotFound(message=str(e))
        except TeamMemberNotFoundError as e:
            return TeamMemberNotFound(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdmin])
    def update_rebus_race_title(
        self, input: UpdateRebusRaceTitleInput
    ) -> UpdateRebusRaceTitleOutput:
        try:
            rebus_race = app.update_rebus_race_title(
                rebus_race_id=UUID(input.id), title=input.title
            )
            return UpdateRebusRaceTitlePayload(rebus_race=rebus_race)
        except RebusRaceNotFoundError as e:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdmin])
    def update_rebus_post_title(
        self, input: UpdateRebusPostTitleInput
    ) -> UpdateRebusPostTitleOutput:
        try:
            rebus_post = app.update_rebus_post_title(
                rebus_race_id=UUID(input.rebus_race_id),
                rebus_post_id=UUID(input.rebus_post_id),
                title=input.title,
            )
            return UpdateRebusPostTitlePayload(rebus_post=rebus_post)
        except RebusRaceNotFoundError as e:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))
        except RebusPostNotFoundError as e:
            return RebusPostNotFound(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdminOrUser])
    def update_rebus_post_question_team_answer(
        self, input: UpdateRebusPostQuestionTeamAnswerInput
    ) -> UpdateRebusPostQuestionTeamAnswerOutput:
        try:
            rebus_post_question = app.update_rebus_post_question_team_answer(
                rebus_race_id=UUID(input.rebus_race_id),
                rebus_post_id=UUID(input.rebus_post_id),
                rebus_post_question_id=UUID(input.rebus_post_question_id),
                team_id=UUID(input.team_id),
                answer=input.answer,
            )
            return UpdateRebusPostQuestionTeamAnswerPayload(
                rebus_post_question=rebus_post_question
            )
        except RebusRaceNotFoundError as e:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))
        except RebusPostNotFoundError as e:
            return RebusPostNotFound(message=str(e))
        except RebusPostQuestionNotFoundError as e:
            return RebusPostQuestionNotFound(message=str(e))
        except TeamNotFoundError as e:
            return TeamMemberNotFound(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdmin])
    def update_rebus_post_question_team_answer_score(
        self, input: UpdateRebusPostQuestionTeamAnswerScoreInput
    ) -> UpdateRebusPostQuestionTeamAnswerScoreOutput:
        try:
            rebus_post_question = app.update_rebus_post_question_team_answer_score(
                rebus_race_id=UUID(input.rebus_race_id),
                rebus_post_id=UUID(input.rebus_post_id),
                rebus_post_question_id=UUID(input.rebus_post_question_id),
                team_id=UUID(input.team_id),
                score=input.score,
            )
            return UpdateRebusPostQuestionTeamAnswerScorePayload(
                rebus_post_question=rebus_post_question
            )
        except RebusRaceNotFoundError as e:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))
        except RebusPostNotFoundError as e:
            return RebusPostNotFound(message=str(e))
        except RebusPostQuestionNotFoundError as e:
            return RebusPostQuestionNotFound(message=str(e))
        except TeamNotFoundError as e:
            return TeamMemberNotFound(message=str(e))

    @strawberry.mutation(permission_classes=[IsAdminOrUser])
    def update_team_name(self, input: UpdateTeamNameInput) -> UpdateTeamNameOutput:
        try:
            team = app.update_team_name(
                rebus_race_id=UUID(input.rebus_race_id),
                team_id=UUID(input.team_id),
                name=input.name,
            )
            return UpdateTeamNamePayload(team=team)
        except RebusRaceNotFoundError as e:
            return RebusRaceNotFound(message=str(e))
        except RebusRaceLockedError as e:
            return RebusRaceLocked(message=str(e))
        except TeamNotFoundError as e:
            return TeamNotFound(message=str(e))


schema = strawberry.Schema(query=Query, mutation=Mutation)
router = GraphQL(schema)
