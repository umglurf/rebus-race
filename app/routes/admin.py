# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from uuid import UUID
from authlib.integrations.starlette_client import OAuth, OAuthError
from fastapi import APIRouter, Depends, HTTPException, Form, Request, UploadFile
from fastapi.responses import HTMLResponse, RedirectResponse, StreamingResponse
from fastapi.templating import Jinja2Templates
import logging
from starlette.responses import RedirectResponse
from fastapi_csrf_protect import CsrfProtect
import qrcode
import qrcode.image.svg
from pydantic import EmailStr

from ..application import get_application
from ..domain import (
    MediaAsset,
    RebusPost,
    RebusPostLocation,
    RebusPostQuestion,
    Team,
    TeamMember,
)
from ..config import get_settings
from ..dependencies import is_admin, is_logged_in_admin
from ..exceptions import (
    RebusRaceLockedError,
    RebusRaceNotFoundError,
    RebusPostNotFoundError,
)
from typing import IO, List, Optional, Union

from ..util import FlashMessage, flash, get_flashed_messages

settings = get_settings()

oauth = OAuth()
oauth.register(
    "adminauth",
    client_id=settings.admin_oauth_client_id,
    client_secret=settings.admin_oauth_client_secret,
    server_metadata_url=settings.admin_oauth_metadata_url,
    client_kwargs={"scope": "openid profile email"},
)

app = get_application()

router = APIRouter()

logger = logging.getLogger("admin")

templates = Jinja2Templates(directory="templates")
templates.env.globals["get_flashed_messages"] = get_flashed_messages


@router.get("/login", response_class=HTMLResponse)
async def login(request: Request):
    redirect_uri = str(request.url_for("auth"))
    return await oauth.adminauth.authorize_redirect(request, redirect_uri)


@router.get("/logout", response_class=HTMLResponse)
async def logout(request: Request):
    if is_logged_in_admin(request=request):
        request.session.pop("adminuser")
    return RedirectResponse("/")


@router.get("/auth")
async def auth(request: Request):
    try:
        token = await oauth.adminauth.authorize_access_token(request)
    except OAuthError as e:
        return HTTPException(status_code=500, detail=e.error)
    request.session["adminuser"] = token["userinfo"].get("email", "")

    return RedirectResponse(url="/")


@router.get("/", response_class=HTMLResponse)
async def index(request: Request, csrf_protect: CsrfProtect = Depends()):
    rebus_races = list(app.get_rebus_races())
    logged_in = is_logged_in_admin(request=request)
    admin = False
    try:
        await is_admin(request)
        admin = True
    except HTTPException:
        pass
    response = templates.TemplateResponse(
        "index.html",
        context={
            "request": request,
            "rebus_races": rebus_races,
            "is_admin": admin,
            "is_logged_in": logged_in,
        },
    )
    csrf_protect.set_csrf_cookie(response)
    return response


@router.post(
    "/rebus_race", dependencies=[Depends(is_admin)], response_class=HTMLResponse
)
async def create_rebus_race(
    request: Request, csrf_protect: CsrfProtect = Depends(), title: str = Form()
):
    try:
        csrf_protect.validate_csrf_in_cookies(request)
        app.create_rebus_race(title)
        flash(
            request=request,
            message=FlashMessage(message="Rebus race created", category="success"),
        )
    except Exception as e:
        logger.error(e)
        flash(
            request=request,
            message=FlashMessage(message="Error creating rebus race", category="error"),
        )

    return RedirectResponse("/", status_code=303)


def get_team_url_qrcode(base_url: str, team_id: UUID) -> str:
    factory = qrcode.image.svg.SvgPathImage
    img = qrcode.make(f"{base_url}/team/{team_id}", image_factory=factory)
    return img.to_string(encoding="unicode")


@router.get(
    "/rebus_race/{rebus_race_id}",
    dependencies=[Depends(is_admin)],
    response_class=HTMLResponse,
)
async def rebus_race(
    request: Request, rebus_race_id: UUID, csrf_protect: CsrfProtect = Depends()
):
    try:
        rebus_race = app.get_rebus_race(rebus_race_id=rebus_race_id)
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
        return RedirectResponse("/")

    logged_in = is_logged_in_admin(request=request)
    response = templates.TemplateResponse(
        "rebus_race.html",
        context={
            "request": request,
            "rebus_race": rebus_race,
            "get_team_url_qrcode": get_team_url_qrcode,
            "is_logged_in": logged_in,
        },
    )
    csrf_protect.set_csrf_cookie(response)
    return response


@router.post(
    "/rebus_race/{rebus_race_id}/rebus_post",
    dependencies=[Depends(is_admin)],
    response_class=HTMLResponse,
)
async def create_rebus_post(
    request: Request,
    rebus_race_id: UUID,
    csrf_protect: CsrfProtect = Depends(),
    title: str = Form(),
    location_hint: Optional[str] = Form(default=None),
    latitude: Optional[float] = Form(default=None),
    longitude: Optional[float] = Form(default=None),
    leeway: Optional[int] = Form(default=0),
):
    rebus_post_location = None
    if location_hint is not None:
        rebus_post_location = RebusPostLocation(
            location_hint=location_hint,
            latitude=latitude,
            longitude=longitude,
            leeway=leeway,
        )
    rebus_post = RebusPost(title=title, location=rebus_post_location)
    try:
        csrf_protect.validate_csrf_in_cookies(request)
        app.add_rebus_post(rebus_race_id=rebus_race_id, rebus_post=rebus_post)
        flash(
            request=request,
            message=FlashMessage(message="Rebus post added", category="success"),
        )
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
    except RebusRaceLockedError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race is locked", category="error"),
        )
    except Exception as e:
        logger.error(e)
        flash(
            request=request,
            message=FlashMessage(message="Error creating rebus post", category="error"),
        )
    return RedirectResponse(f"/rebus_race/{rebus_race_id}", status_code=303)


@router.get(
    "/rebus_race/{rebus_race_id}/rebus_post/{rebus_post_id}",
    dependencies=[Depends(is_admin)],
    response_class=HTMLResponse,
)
async def get_rebus_post(
    request: Request,
    rebus_race_id: UUID,
    rebus_post_id: UUID,
    csrf_protect: CsrfProtect = Depends(),
):
    try:
        rebus_race = app.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_post = [
            rebus_post
            for rebus_post in rebus_race.rebus_posts
            if rebus_post.id == rebus_post_id
        ]
        if len(rebus_post) != 1:
            flash(
                request=request,
                message=FlashMessage(
                    message="Rebus post does not exist", category="error"
                ),
            )
            return RedirectResponse(f"/rebus_race/{rebus_race_id}")
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
        return RedirectResponse("/")

    logged_in = is_logged_in_admin(request=request)
    response = templates.TemplateResponse(
        "rebus_post.html",
        context={
            "request": request,
            "rebus_race": rebus_race,
            "rebus_post": rebus_post[0],
            "is_logged_in": logged_in,
        },
    )
    csrf_protect.set_csrf_cookie(response)
    return response


@router.get(
    "/rebus_race/{rebus_race_id}/rebus_post/{rebus_post_id}/media/{media_id}",
    response_class=HTMLResponse,
)
async def get_rebus_post_media(
    request: Request,
    rebus_race_id: UUID,
    rebus_post_id: UUID,
    media_id: UUID,
    csrf_protect: CsrfProtect = Depends(),
):
    try:
        rebus_race = app.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_post = [
            rebus_post
            for rebus_post in rebus_race.rebus_posts
            if rebus_post.id == rebus_post_id
        ]
        if len(rebus_post) != 1:
            flash(
                request=request,
                message=FlashMessage(
                    message="Rebus post does not exist", category="error"
                ),
            )
            return RedirectResponse(f"/rebus_race/{rebus_race_id}")
        media = [
            m
            for question in rebus_post[0].questions
            for m in question.media
            if m.id == media_id
        ]
        if len(media) != 1:
            flash(
                request=request,
                message=FlashMessage(message="Media does not exist", category="error"),
            )
            return RedirectResponse(
                f"/rebus_race/{rebus_race_id}/rebus_post/{rebus_post_id}"
            )

    except RebusRaceLockedError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race is locked", category="error"),
        )
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
        return RedirectResponse("/")

    def iterfile():
        yield media[0].data

    return StreamingResponse(iterfile(), media_type=media[0].mime_type)


@router.post(
    "/rebus_race/{rebus_race_id}/rebus_post/{rebus_post_id}/location",
    dependencies=[Depends(is_admin)],
    response_class=HTMLResponse,
)
async def update_rebus_post_location(
    request: Request,
    rebus_race_id: UUID,
    rebus_post_id: UUID,
    csrf_protect: CsrfProtect = Depends(),
    location_hint: str = Form(),
    latitude: Optional[float] = Form(default=None),
    longitude: Optional[float] = Form(default=None),
    leeway: Optional[int] = Form(default=0),
):
    rebus_post_location = RebusPostLocation(
        location_hint=location_hint,
        latitude=latitude,
        longitude=longitude,
        leeway=leeway,
    )
    try:
        csrf_protect.validate_csrf_in_cookies(request)
        app.update_rebus_post_location(
            rebus_race_id=rebus_race_id,
            rebus_post_id=rebus_post_id,
            location=rebus_post_location,
        )
        flash(
            request=request,
            message=FlashMessage(
                message="Rebus post location updated", category="success"
            ),
        )
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
    except RebusRaceLockedError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race is locked", category="error"),
        )
    except RebusPostNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus post does not exist", category="error"),
        )
    except Exception as e:
        logger.error(e)
        flash(
            request=request,
            message=FlashMessage(
                message="Error updating rebus post location", category="error"
            ),
        )
    return RedirectResponse(
        f"/rebus_race/{rebus_race_id}/rebus_post/{rebus_post_id}", status_code=303
    )


@router.post(
    "/rebus_race/{rebus_race_id}/rebus_post/{rebus_post_id}/question",
    dependencies=[Depends(is_admin)],
    response_class=HTMLResponse,
)
async def add_rebus_post_question(
    request: Request,
    rebus_race_id: UUID,
    rebus_post_id: UUID,
    csrf_protect: CsrfProtect = Depends(),
    question: str = Form(),
    answer: Optional[str] = Form(default=None),
    media: Union[List[UploadFile], None] = None,
):
    if media is None:
        assets = None
    else:
        assets = []
        for media_file in media:
            file_data = await media_file.read()
            assets.append(MediaAsset(data=file_data, mime_type=media_file.content_type))
    post_question = RebusPostQuestion(question=question, answer=answer, media=assets)
    try:
        csrf_protect.validate_csrf_in_cookies(request)
        app.add_rebus_post_question(
            rebus_race_id=rebus_race_id,
            rebus_post_id=rebus_post_id,
            question=post_question,
        )
        flash(
            request=request,
            message=FlashMessage(
                message="Rebus post location updated", category="success"
            ),
        )
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
    except RebusRaceLockedError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race is locked", category="error"),
        )
    except RebusPostNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus post does not exist", category="error"),
        )
    except Exception as e:
        logger.error(e)
        flash(
            request=request,
            message=FlashMessage(
                message="Error adding rebus post question", category="error"
            ),
        )
    return RedirectResponse(
        f"/rebus_race/{rebus_race_id}/rebus_post/{rebus_post_id}", status_code=303
    )


@router.post(
    "/rebus_race/{rebus_race_id}/team",
    dependencies=[Depends(is_admin)],
    response_class=HTMLResponse,
)
async def create_team(
    request: Request,
    rebus_race_id: UUID,
    csrf_protect: CsrfProtect = Depends(),
    name: str = Form(),
):
    team = Team(name=name)
    try:
        csrf_protect.validate_csrf_in_cookies(request)
        app.add_team(rebus_race_id=rebus_race_id, team=team)
        flash(
            request=request,
            message=FlashMessage(message="Team added", category="success"),
        )
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
    except RebusRaceLockedError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race is locked", category="error"),
        )
    except Exception as e:
        logger.error(e)
        flash(
            request=request,
            message=FlashMessage(message="Error creating team", category="error"),
        )
    return RedirectResponse(f"/rebus_race/{rebus_race_id}", status_code=303)


@router.post(
    "/rebus_race/{rebus_race_id}/team/{team_id}/member",
    dependencies=[Depends(is_admin)],
    response_class=HTMLResponse,
)
async def create_team_member(
    request: Request,
    rebus_race_id: UUID,
    team_id: UUID,
    csrf_protect: CsrfProtect = Depends(),
    name: str = Form(),
    email: Optional[EmailStr] = Form(default=None),
):
    member = TeamMember(name=name, email=email)
    try:
        csrf_protect.validate_csrf_in_cookies(request)
        app.add_team_members(
            rebus_race_id=rebus_race_id, team_id=team_id, members=[member]
        )
        flash(
            request=request,
            message=FlashMessage(message="Team member added", category="success"),
        )
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
    except RebusRaceLockedError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race is locked", category="error"),
        )
    except Exception as e:
        logger.error(e)
        flash(
            request=request,
            message=FlashMessage(message="Error creating team member", category="error"),
        )
    return RedirectResponse(f"/rebus_race/{rebus_race_id}", status_code=303)
