# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from uuid import UUID
from authlib.integrations.starlette_client import OAuth, OAuthError
from fastapi import APIRouter, Depends, HTTPException, Form, Request
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
import logging
from starlette.responses import RedirectResponse
from fastapi_csrf_protect import CsrfProtect

from ..application import get_application
from ..domain import (
    MediaAsset,
    RebusPost,
    RebusPostLocation,
    RebusPostQuestion,
    Team,
    TeamMember,
)
from ..exceptions import (
    RebusRaceLockedError,
    RebusRaceNotFoundError,
    RebusPostNotFoundError,
    TeamNotFoundError,
    UnlockCodeWrongError,
)
from typing import List, Optional, Union

from ..config import get_settings
from ..dependencies import is_logged_in_user, is_valid_user
from ..util import FlashMessage, flash, get_flashed_messages

settings = get_settings()

oauth = OAuth()
oauth.register(
    "userauth",
    client_id=settings.user_oauth_client_id,
    client_secret=settings.user_oauth_client_secret,
    server_metadata_url=settings.user_oauth_metadata_url,
    client_kwargs={"scope": "openid profile email"},
)

app = get_application()

router = APIRouter()

logger = logging.getLogger("team")

templates = Jinja2Templates(directory="templates")
templates.env.globals["get_flashed_messages"] = get_flashed_messages


@router.get("/teamlogin", response_class=HTMLResponse)
async def team_login(request: Request):
    redirect_uri = str(request.url_for("team_auth"))
    return await oauth.userauth.authorize_redirect(request, redirect_uri)


@router.get("/teamlogout", response_class=HTMLResponse)
async def team_logout(request: Request):
    if is_logged_in_user(request=request):
        request.session.pop("user")
    rebus_race_id = request.session.get("rebus_race_id", "")
    team_id = request.session.get("team_id", "")
    return RedirectResponse(url=f"/rebus_race/{rebus_race_id}/team/{team_id}")


@router.get("/teamauth")
async def team_auth(request: Request):
    try:
        token = await oauth.userauth.authorize_access_token(request)
    except OAuthError as e:
        return HTTPException(status_code=500, detail=e.error)
    request.session["user"] = token["userinfo"].get("email", "")

    rebus_race_id = request.session.get("rebus_race_id", "")
    team_id = request.session.get("team_id", "")
    return RedirectResponse(url=f"/rebus_race/{rebus_race_id}/team/{team_id}")


@router.get(
    "/rebus_race/{rebus_race_id}/team/{team_id}",
    response_class=HTMLResponse,
)
async def get_team(
    request: Request,
    rebus_race_id: UUID,
    team_id: UUID,
    csrf_protect: CsrfProtect = Depends(),
):
    request.session["rebus_race_id"] = str(rebus_race_id)
    request.session["team_id"] = str(team_id)
    logged_in = is_logged_in_user(request=request)
    valid_user = False
    try:
        await is_valid_user(
            request=request, rebus_race_id=rebus_race_id, team_id=team_id
        )
        valid_user = True
    except HTTPException:
        pass
    try:
        rebus_race = app.get_rebus_race(rebus_race_id=rebus_race_id)
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
        return RedirectResponse("/")
    team = [team for team in rebus_race.teams if team.id == team_id]
    if len(team) != 1:
        flash(
            request=request,
            message=FlashMessage(message="Team does not exist", category="error"),
        )
        return RedirectResponse("/")
    response = templates.TemplateResponse(
        "team.html",
        context={
            "request": request,
            "rebus_race": rebus_race,
            "team": team[0],
            "is_logged_in": logged_in,
            "is_valid_user": valid_user,
        },
    )
    csrf_protect.set_csrf_cookie(response)
    return response


@router.post(
    "/rebus_race/{rebus_race_id}/team/{team_id}/rebus_post/{rebus_post_id}/unlock",
    dependencies=[Depends(is_valid_user)],
    response_class=HTMLResponse,
)
async def team_unlock(
    request: Request,
    rebus_race_id: UUID,
    rebus_post_id: UUID,
    team_id: UUID,
    unlock_code: Optional[str] = Form(default=None),
    latitude: Optional[float] = Form(default=None),
    longitude: Optional[float] = Form(default=None),
    csrf_protect: CsrfProtect = Depends(),
):
    try:
        csrf_protect.validate_csrf_in_cookies(request)
        app.unlock_rebus_post(
            rebus_race_id=rebus_race_id,
            rebus_post_id=rebus_post_id,
            team_id=team_id,
            unlock_code=unlock_code,
            latitude=latitude,
            longitude=longitude,
        )
    except RebusRaceNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
        return RedirectResponse("/", status_code=303)
    except RebusRaceLockedError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race is locked", category="error"),
        )
        return RedirectResponse("/", status_code=303)
    except TeamNotFoundError:
        flash(
            request=request,
            message=FlashMessage(message="Rebus race does not exist", category="error"),
        )
        return RedirectResponse("/", status_code=303)
    except RebusPostNotFoundError:
        flash(
            request=request,
            message=FlashMessage(
                message="Rebus race post does not exist", category="error"
            ),
        )
        return RedirectResponse(
            f"/rebus_race/{rebus_race_id}/team/{team_id}", status_code=303
        )
    except UnlockCodeWrongError:
        flash(
            request=request,
            message=FlashMessage(
                message="Wrong unlock code or location", category="warning"
            ),
        )
        return RedirectResponse(
            f"/rebus_race/{rebus_race_id}/team/{team_id}", status_code=303
        )
    except Exception as e:
        logger.error(e)
        flash(
            request=request,
            message=FlashMessage(message="Error unlocking", category="danger"),
        )
        return RedirectResponse(
            f"/rebus_race/{rebus_race_id}/team/{team_id}", status_code=303
        )
    flash(
        request=request,
        message=FlashMessage(message="Post unlocked", category="info"),
    )
    return RedirectResponse(
        f"/rebus_race/{rebus_race_id}/team/{team_id}", status_code=303
    )
