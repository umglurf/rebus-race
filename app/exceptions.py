# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only


class RebusRaceError(Exception):
    pass


class RebusRaceNotFoundError(RebusRaceError):
    pass


class RebusRaceLockedError(RebusRaceError):
    pass


class RebusPostError(RebusRaceError):
    pass


class RebusPostNotFoundError(RebusRaceError):
    pass


class RebusPostQuestionNotFoundError(RebusRaceError):
    pass


class RebusPostQuestionTeamAnswerNotFoundError(RebusRaceError):
    pass


class TeamNotFoundError(RebusRaceError):
    pass


class TeamMemberNotFoundError(RebusRaceError):
    pass


class UnlockCodeWrongError(RebusRaceError):
    pass
