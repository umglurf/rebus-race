# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from fastapi import FastAPI
from fastapi.middleware import Middleware
from fastapi.staticfiles import StaticFiles
from pydantic.main import BaseModel
from starlette import middleware
from fastapi_csrf_protect import CsrfProtect
from fastapi_csrf_protect.exceptions import CsrfProtectError
import logging
from os import environ
from starlette.middleware.sessions import SessionMiddleware
from uvicorn.middleware.proxy_headers import ProxyHeadersMiddleware

from .config import get_settings
from .routes import admin, graphql, team

settings = get_settings()

middleware = [
    Middleware(SessionMiddleware, secret_key=settings.secret_key),
    Middleware(
        ProxyHeadersMiddleware,
        trusted_hosts=environ.get("FORWARDED_ALLOW_IPS", ["127.0.0.1", "::1"]),
    ),
]

app = FastAPI(middleware=middleware)

logging.basicConfig()
logger = logging.getLogger("main")


class CsrfSettings(BaseModel):
    secret_key: str


@CsrfProtect.load_config
def get_csrf_config():
    return CsrfSettings(secret_key=settings.secret_key)


@app.exception_handler(CsrfProtectError)
def csrf_protect_exception_handler(exc: CsrfProtectError):
    return JSONResponse(status_code=exc.status_code, content={"detail": exc.message})


app.add_route("/graphql", graphql.router)

app.include_router(admin.router)
app.include_router(team.router)

app.mount("/static", StaticFiles(directory="static"), name="static")
