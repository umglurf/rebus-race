# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from functools import lru_cache
from typing import List, Optional
from pydantic import BaseSettings, EmailStr


class Settings(BaseSettings):
    secret_key: str
    admin_oauth_client_id: Optional[str] = None
    admin_oauth_client_secret: Optional[str] = None
    admin_oauth_metadata_url: Optional[str] = None
    admin_valid_emails: Optional[List[EmailStr]] = None
    user_oauth_client_id: Optional[str] = None
    user_oauth_client_secret: Optional[str] = None
    user_oauth_metadata_url: Optional[str] = None


@lru_cache
def get_settings():
    return Settings()
