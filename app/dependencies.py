# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from fastapi import HTTPException, Request
from uuid import UUID

from .application import get_application
from .config import get_settings
from .exceptions import RebusRaceNotFoundError

app = get_application()
settings = get_settings()


def is_logged_in_admin(request: Request) -> bool:
    session = request.session
    if not session or "adminuser" not in session:
        return False
    return True


def is_logged_in_user(request: Request) -> bool:
    session = request.session
    if not session or "user" not in session:
        return False
    return True


async def is_admin(request: Request) -> None:
    if (
        settings.admin_oauth_client_id is None
        and settings.admin_oauth_client_secret is None
        and settings.admin_oauth_metadata_url is None
    ):
        return
    if not is_logged_in_admin(request=request):
        raise HTTPException(status_code=401, detail="Unauthorized")
    if settings.admin_valid_emails is None:
        return
    if request.session["adminuser"] not in settings.admin_valid_emails:
        raise HTTPException(status_code=401, detail="Unauthorized")


async def is_valid_user(
    request: Request,
    rebus_race_id: UUID,
    team_id: UUID,
) -> None:
    if (
        settings.user_oauth_client_id is None
        and settings.user_oauth_client_secret is None
    ):
        return
    if not is_logged_in_user(request=request):
        raise HTTPException(status_code=401, detail="Unauthorized")
    email = request.session["user"]
    try:
        rebus_race = app.get_rebus_race(rebus_race_id=rebus_race_id)
    except RebusRaceNotFoundError:
        raise HTTPException(status_code=401, detail="Unauthorized")
    for team in rebus_race.teams:
        if team.id != team_id:
            continue
        for member in team.members:
            if member.email == email:
                return
    raise HTTPException(status_code=401, detail="Unauthorized")
