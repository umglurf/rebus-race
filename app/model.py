# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from pydantic import BaseModel
from typing import Dict
from uuid import UUID


class RebusRace(BaseModel):
    title: str


class TeamMember(BaseModel):
    name: str


class Team(BaseModel):
    name: str
    members: Dict[UUID, TeamMember]


class TeamMember(BaseModel):
    name: str
