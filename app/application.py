# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from eventsourcing.application import (
    AggregateNotFound,
    Application,
    EventSourcedLog,
    EnvType,
)
from eventsourcing.domain import DomainEvent
from eventsourcing.persistence import Transcoder
from functools import lru_cache
from typing import List, Optional
from uuid import NAMESPACE_URL, UUID, uuid5

from .domain import (
    MediaAsset,
    MediaAssetTranscoding,
    RebusPostQuestionTeamAnswer,
    RebusRace,
    RebusPost,
    RebusPostTranscoding,
    RebusPostLocation,
    RebusPostLocationTranscoding,
    RebusPostQuestion,
    RebusPostQuestionTranscoding,
    Team,
    TeamTranscoding,
    TeamMember,
    TeamMemberTranscoding,
)
from .exceptions import RebusRaceNotFoundError


class RebusRacesLog(DomainEvent):
    rebus_race_id: UUID


class RebusRaces(Application):
    def __init__(self, env: Optional[EnvType] = None) -> None:
        super().__init__(env)
        self.rebus_races_log = EventSourcedLog(
            events=self.events,
            originator_id=uuid5(NAMESPACE_URL, "/rebusraces"),
            logged_cls=RebusRacesLog,
        )

    def add_rebus_post(self, rebus_race_id: UUID, rebus_post: RebusPost) -> RebusPost:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.add_rebus_post(rebus_post)
        self.save(rebus_race)
        return rebus_post

    def add_rebus_post_question(
        self, rebus_race_id: UUID, rebus_post_id: UUID, question: RebusPostQuestion
    ) -> RebusPost:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.add_rebus_post_question(
            rebus_post_id=rebus_post_id, question=question
        )
        self.save(rebus_race)
        return [post for post in rebus_race.rebus_posts if post.id == rebus_post_id][0]

    def add_team(self, rebus_race_id: UUID, team: Team) -> Team:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.add_team(team)
        self.save(rebus_race)
        return team

    def add_team_members(
        self, rebus_race_id: UUID, team_id: UUID, members: List[TeamMember]
    ) -> Team:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.add_team_members(team_id=team_id, members=members)
        self.save(rebus_race)
        return [team for team in rebus_race.teams if team.id == team_id][0]

    def create_rebus_race(self, title: str) -> RebusRace:
        rebus_race = RebusRace(title)
        rebus_race_logged = self.rebus_races_log.trigger_event(
            rebus_race_id=rebus_race.id
        )
        self.save(rebus_race, rebus_race_logged)
        return rebus_race

    def get_rebus_race(self, rebus_race_id: UUID) -> RebusRace:
        try:
            rebus_race: RebusRace = self.repository.get(rebus_race_id)
            return rebus_race
        except AggregateNotFound as e:
            raise RebusRaceNotFoundError(f"no rebus race with id {rebus_race_id}")

    def get_rebus_races(self) -> List[RebusRace]:
        for rebus_race_logged in self.rebus_races_log.get():
            yield self.get_rebus_race(rebus_race_logged.rebus_race_id)

    def lock_rebus_race(self, rebus_race_id: UUID) -> None:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.lock()
        self.save(rebus_race)
        return rebus_race

    def remove_rebus_post(self, rebus_race_id: UUID, rebus_post_id: UUID) -> RebusRace:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.remove_rebus_post(rebus_post_id=rebus_post_id)
        self.save(rebus_race)
        return rebus_race

    def remove_rebus_post_question(
        self, rebus_race_id: UUID, rebus_post_id: UUID, question_id: UUID
    ) -> RebusRace:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.remove_rebus_post_question(
            rebus_post_id=rebus_post_id, question_id=question_id
        )
        self.save(rebus_race)
        return rebus_race

    def remove_team(self, rebus_race_id: UUID, team_id: UUID) -> RebusRace:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.remove_team(team_id=team_id)
        self.save(rebus_race)
        return rebus_race

    def remove_team_member(
        self, rebus_race_id: UUID, team_id: UUID, team_member_id: UUID
    ) -> Team:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.remove_team_member(team_id=team_id, team_member_id=team_member_id)
        self.save(rebus_race)
        return [team for team in rebus_race.teams if team.id == team_id][0]

    def unlock_rebus_race(self, rebus_race_id: UUID) -> None:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.unlock()
        self.save(rebus_race)
        return rebus_race

    def update_rebus_race_title(self, rebus_race_id: UUID, title: str) -> RebusRace:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.update_title(title)
        self.save(rebus_race)
        return rebus_race

    def update_rebus_post_location(
        self, rebus_race_id: UUID, rebus_post_id: UUID, location: RebusPostLocation
    ) -> RebusPost:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.update_rebus_post_location(
            rebus_post_id=rebus_post_id, location=location
        )
        self.save(rebus_race)
        return [
            rebus_post
            for rebus_post in rebus_race.rebus_posts
            if rebus_post.id == rebus_post_id
        ][0]

    def update_rebus_post_title(
        self, rebus_race_id: UUID, rebus_post_id: UUID, title: str
    ) -> RebusPost:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.update_rebus_post_title(rebus_post_id=rebus_post_id, title=title)
        self.save(rebus_race)
        return [
            rebus_post
            for rebus_post in rebus_race.rebus_posts
            if rebus_post.id == rebus_post_id
        ][0]

    def update_rebus_post_question_team_answer(
        self,
        rebus_race_id: UUID,
        rebus_post_id: UUID,
        rebus_post_question_id: UUID,
        team_id: UUID,
        answer=str,
    ) -> RebusPostQuestion:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.update_rebus_post_question_team_answer(
            rebus_post_id=rebus_post_id,
            rebus_post_question_id=rebus_post_question_id,
            team_id=team_id,
            answer=answer,
        )
        self.save(rebus_race)
        rebus_post = [
            rebus_post
            for rebus_post in rebus_race.rebus_posts
            if rebus_post.id == rebus_post_id
        ][0]
        return [
            question
            for question in rebus_post.questions
            if question.id == rebus_post_question_id
        ][0]

    def update_rebus_post_question_team_answer_score(
        self,
        rebus_race_id: UUID,
        rebus_post_id: UUID,
        rebus_post_question_id: UUID,
        team_id: UUID,
        score: float,
    ) -> RebusPostQuestion:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.update_rebus_post_question_team_answer_score(
            rebus_post_id=rebus_post_id,
            rebus_post_question_id=rebus_post_question_id,
            team_id=team_id,
            score=score,
        )
        self.save(rebus_race)
        rebus_post = [
            rebus_post
            for rebus_post in rebus_race.rebus_posts
            if rebus_post.id == rebus_post_id
        ][0]
        return [
            question
            for question in rebus_post.questions
            if question.id == rebus_post_question_id
        ][0]

    def update_team_name(self, rebus_race_id: UUID, team_id: UUID, name: str) -> Team:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        team = rebus_race.update_team_name(team_id=team_id, name=name)
        self.save(rebus_race)
        return [team for team in rebus_race.teams if team.id == team_id][0]

    def unlock_rebus_post(
        self,
        rebus_race_id: UUID,
        rebus_post_id: UUID,
        team_id: UUID,
        unlock_code: Optional[str] = None,
        latitude: Optional[float] = None,
        longitude: Optional[float] = None,
    ) -> RebusPost:
        rebus_race = self.get_rebus_race(rebus_race_id=rebus_race_id)
        rebus_race.unlock_rebus_post(
            rebus_race_id=rebus_race_id,
            rebus_post_id=rebus_post_id,
            team_id=team_id,
            unlock_code=unlock_code,
            latitude=latitude,
            longitude=longitude,
        )
        self.save(rebus_race)
        return [
            rebus_post
            for rebus_post in rebus_race.rebus_posts
            if rebus_post.id == rebus_post_id
        ][0]

    def register_transcodings(self, transcoder: Transcoder) -> None:
        super().register_transcodings(transcoder)
        transcoder.register(MediaAssetTranscoding())
        transcoder.register(RebusPostTranscoding())
        transcoder.register(RebusPostLocationTranscoding())
        transcoder.register(RebusPostQuestionTranscoding())
        transcoder.register(TeamTranscoding())
        transcoder.register(TeamMemberTranscoding())


@lru_cache
def get_application(env: Optional[EnvType] = None):
    return RebusRaces(env=env)
