# SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>
#
# SPDX-License-Identifier: GPL-3.0-only

from __future__ import annotations

from base64 import b64decode, b64encode
from eventsourcing.domain import Aggregate, event
from eventsourcing.persistence import Transcoding
from pygc import great_distance
from typing import Any, Dict, List, Optional
import secrets
import strawberry
from strawberry.file_uploads import Upload
from uuid import uuid4

from .exceptions import (
    RebusRaceLockedError,
    RebusPostError,
    RebusPostNotFoundError,
    RebusPostQuestionNotFoundError,
    RebusPostQuestionTeamAnswerNotFoundError,
    TeamMemberNotFoundError,
    TeamNotFoundError,
    UnlockCodeWrongError,
)


@strawberry.type
class MediaAsset:
    id: strawberry.ID
    data: Upload
    mime_type: str

    def __init__(
        self, data: Upload, mime_type: str, id: Optional[strawberry.ID] = None
    ) -> None:
        if id is None:
            self.id = uuid4()
        else:
            self.id = id
        self.data = data
        self.mime_type = mime_type


class MediaAssetTranscoding(Transcoding):
    type = MediaAsset
    name = "MediaAsset"

    def encode(self, obj: MediaAsset) -> Dict[str, Any]:
        d = obj.__dict__
        d["data"] = b64encode(d["data"]).decode("ascii")
        return d

    def decode(self, data: Dict[str, Any]) -> MediaAsset:
        data["data"] = b64decode(data["data"])
        return MediaAsset(**data)


@strawberry.type
class RebusRace(Aggregate):
    _id: strawberry.ID
    title: str
    teams: List[Team]
    rebus_posts: List[RebusPost]
    locked: bool

    @event("RegisterRebusRace")
    def __init__(self, title: str) -> None:
        self.title = title
        self.teams = []
        self.rebus_posts = []
        self.locked = False

    @event("AddRebusRacePost")
    def add_rebus_post(self, rebus_post: RebusPost) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        self.rebus_posts.append(rebus_post)

    @event("AddRebusRacePostQuestion")
    def add_rebus_post_question(
        self, rebus_post_id: strawberry.ID, question: RebusPostQuestion
    ) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        post = [post for post in self.rebus_posts if post.id == rebus_post_id]
        if len(post) != 1:
            raise RebusPostNotFoundError(f"no rebus post with id {rebus_post_id}")
        post[0].questions.append(question)

    @event("AddRebusRaceTeam")
    def add_team(self, team: Team) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        team.team_number = len(self.teams)
        self.teams.append(team)

    @event("AddTeamMembers")
    def add_team_members(
        self, team_id: strawberry.ID, members: List[TeamMember]
    ) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        for member in members:
            team[0].add_team_member(member)

    def get_next_team_post(self, team_id: strawberry.ID):
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        team = team[0]

        if team.unlocked_posts is not None and len(team.unlocked_posts) == len(
            self.rebus_posts
        ):
            return None

        post_offset = team.team_number
        if team.unlocked_posts is not None:
            post_offset = team.team_number + len(team.unlocked_posts)
        return self.rebus_posts[post_offset % len(self.rebus_posts)]

    def get_team_score(self, team_id: strawberry.ID):
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        team = team[0]

        score = 0
        for rebus_post in self.rebus_posts:
            for question in rebus_post.questions:
                if team not in question.team_answers:
                    continue
                score += question.team_answers[team].score

        return score

    @event("LockRebusRace")
    def lock(self):
        self.locked = True

    @event("RemoveRebusRacePost")
    def remove_rebus_post(self, rebus_post_id: strawberry.ID) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        post = [post for post in self.rebus_posts if post.id == rebus_post_id]
        if len(post) != 1:
            raise RebusPostNotFoundError(f"no rebus post with id {rebus_post_id}")
        self.rebus_posts.remove(post[0])

    @event("RemoveRebusRacePostQuestion")
    def remove_rebus_post_question(
        self, rebus_post_id: strawberry.ID, question_id: strawberry.ID
    ) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        post = [post for post in self.rebus_posts if post.id == rebus_post_id]
        if len(post) != 1:
            raise RebusPostNotFoundError(f"no rebus post with id {rebus_post_id}")
        question = [
            question for question in post[0].questions if question.id == question_id
        ]
        if len(question) != 1:
            raise RebusPostQuestionNotFoundError(f"no question with id {question_id}")
        post[0].questions.remove(question[0])

    @event("RemoveRebusRaceTeam")
    def remove_team(self, team_id: strawberry.ID) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        self.teams.remove(team[0])
        for idx, team in enumerate(self.teams):
            team.team_number = idx

    @event("RemoveTeamMember")
    def remove_team_member(
        self, team_id: strawberry.ID, team_member_id: strawberry.ID
    ) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        team_member = [
            team_member
            for team_member in team[0].members
            if team_member.id == team_member_id
        ]
        if len(team_member) != 1:
            raise TeamMemberNotFoundError(
                f"no team member with id {team_member_id} in team {team_id}"
            )
        team[0].remove_team_member(team_member[0])

    @event("UnlockRebusRace")
    def unlock(self):
        self.locked = False

    @event("UpdateRebusRaceTitle")
    def update_title(self, title: str) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        self.title = title

    @event("UpdateRebusPostLocation")
    def update_rebus_post_location(
        self, location: RebusPostLocation, rebus_post_id: strawberry.ID
    ) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        rebus_post = [
            rebus_post
            for rebus_post in self.rebus_posts
            if rebus_post.id == rebus_post_id
        ]
        if len(rebus_post) != 1:
            raise RebusPostNotFoundError(f"no rebus post with id {rebus_post_id}")
        rebus_post[0].location = location

    @event("UpdateRebusPostTitle")
    def update_rebus_post_title(self, title: str, rebus_post_id: strawberry.ID) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        rebus_post = [
            rebus_post
            for rebus_post in self.rebus_posts
            if rebus_post.id == rebus_post_id
        ]
        if len(rebus_post) != 1:
            raise RebusPostNotFoundError(f"no rebus post with id {rebus_post_id}")
        rebus_post[0].title = title

    @event("UpdateRebusPostTeamAnswer")
    def update_rebus_post_question_team_answer(
        self,
        rebus_post_id: strawberry.ID,
        rebus_post_question_id: strawberry.ID,
        team_id: strawberry.ID,
        answer: str,
    ) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        rebus_post = [
            rebus_post
            for rebus_post in self.rebus_posts
            if rebus_post.id == rebus_post_id
        ]
        if len(rebus_post) != 1:
            raise RebusPostNotFoundError(f"no rebus post with id {rebus_post_id}")
        rebus_post_question = [
            question
            for question in rebus_post[0].questions
            if question.id == rebus_post_question_id
        ]
        if len(rebus_post_question) != 1:
            raise RebusPostQuestionNotFoundError(
                f"no rebus post question with id {rebus_post_question_id}"
            )
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        if team[0] in rebus_post_question[0].team_answers:
            rebus_post_question[0].team_answers[team[0]].answer = answer
        else:
            rebus_post_question[0].team_answers[team[0]] = RebusPostQuestionTeamAnswer(
                answer=answer
            )

    @event("UpdateRebusPostTeamAnswerScore")
    def update_rebus_post_question_team_answer_score(
        self,
        rebus_post_id: strawberry.ID,
        rebus_post_question_id: strawberry.ID,
        team_id: strawberry.ID,
        score: float,
    ) -> None:
        rebus_post = [
            rebus_post
            for rebus_post in self.rebus_posts
            if rebus_post.id == rebus_post_id
        ]
        if len(rebus_post) != 1:
            raise RebusPostNotFoundError(f"no rebus post with id {rebus_post_id}")
        rebus_post_question = [
            question
            for question in rebus_post[0].questions
            if question.id == rebus_post_question_id
        ]
        if len(rebus_post_question) != 1:
            raise RebusPostQuestionNotFoundError(
                f"no rebus post question with id {rebus_post_question_id}"
            )
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        if not team[0] in rebus_post_question[0].team_answers:
            raise RebusPostQuestionTeamAnswerNotFoundError(
                f"No team answer for team {team.id} on question"
            )
        rebus_post_question[0].team_answers[team[0]].score = score

    @event("UpdateTeamName")
    def update_team_name(self, name: str, team_id: strawberry.ID) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        team[0].name = name

    @event("UpdateTeamMemberName")
    def update_team_member_name(self, name: str, team_id: strawberry.ID) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        team_member = [
            team_member
            for team_member in team[0].members
            if team_member.id == team_member_id
        ]
        if len(team_member) != 1:
            raise TeamMemberNotFoundError(
                f"no team member with id {team_member_id} in team {team_id}"
            )
        team_member[0].name = name

    @event("UnlockRebusPost")
    def unlock_rebus_post(
        self,
        rebus_race_id: strawberry.ID,
        rebus_post_id: strawberry.ID,
        team_id: strawberry.ID,
        unlock_code: Optional[str] = None,
        latitude: Optional[float] = None,
        longitude: Optional[float] = None,
    ) -> None:
        if self.locked:
            raise RebusRaceLockedError("Rebus race is locked")
        rebus_post = [
            rebus_post
            for rebus_post in self.rebus_posts
            if rebus_post.id == rebus_post_id
        ]
        if len(rebus_post) != 1:
            raise RebusPostNotFoundError(f"no rebus post with id {rebus_post_id}")
        team = [team for team in self.teams if team.id == team_id]
        if len(team) != 1:
            raise TeamNotFoundError(f"no team with id {team_id}")
        if not rebus_post[0].unlock_post(
            unlock_code=unlock_code, latitude=latitude, longitude=longitude
        ):
            raise UnlockCodeWrongError("wrong unlock code or location")
        if team[0].unlocked_posts is None:
            team[0].unlocked_posts = []
        team[0].unlocked_posts.append(rebus_post[0])


@strawberry.type
class RebusPostLocation:
    id = strawberry.ID
    location_hint: str
    latitude: Optional[float]
    longitude: Optional[float]
    leeway: Optional[int]

    def __init__(
        self,
        location_hint: Optional[str] = None,
        latitude: Optional[float] = None,
        longitude: Optional[float] = None,
        leeway: Optional[int] = 0,
        id: Optional[strawberry.ID] = None,
    ) -> None:
        if id is None:
            self.id = uuid4()
        else:
            self.id = id
        self.location_hint = location_hint
        self.latitude = latitude
        self.longitude = longitude
        self.leeway = leeway

    def unlock_location(self, latitude: float, longitude: float):
        if self.latitude is None or self.longitude is None:
            raise RebusPostError(
                "Can not check location, rebus post has no location coordinates"
            )
        return (
            great_distance(
                start_latitude=self.latitude,
                start_longitude=self.longitude,
                end_latitude=latitude,
                end_longitude=longitude,
            )["distance"]
            <= self.leeway
        )


class RebusPostLocationTranscoding(Transcoding):
    type = RebusPostLocation
    name = "RebusPostLocation"

    def encode(self, obj: RebusPostLocation) -> Dict[str, Any]:
        return obj.__dict__

    def decode(self, data: Dict[str, Any]) -> RebusPostLocation:
        return RebusPostLocation(**data)


@strawberry.type
class RebusPost:
    id: strawberry.ID
    title: str
    unlock_code: str
    location: Optional[RebusPostLocation]
    questions: List[RebusPostQuestion]

    def __init__(
        self,
        title: str,
        location: Optional[RebusPostLocation] = None,
        id: Optional[strawberry.ID] = None,
        questions: Optional[List[RebusPostQuestion]] = None,
        unlock_code: Optional[str] = None,
    ) -> None:
        if id is None:
            self.id = uuid4()
        else:
            self.id = id
        self.title = title
        if unlock_code is None:
            self.unlock_code = "".join(
                [str(secrets.choice(range(0, 9))) for _ in range(1, 7)]
            )
        else:
            self.unlock_code = unlock_code
        self.location = location
        if questions is None:
            self.questions = []
        else:
            self.questions = questions

    def unlock_post(
        self,
        unlock_code: Optional[str] = None,
        longitude: Optional[float] = None,
        latitude: Optional[float] = None,
    ):
        if unlock_code is not None:
            return self.unlock_code == unlock_code
        if latitude is not None and longitude is not None:
            if self.location is None:
                raise RebusPostError(
                    "Trying to check location, but location is not set"
                )
            return self.location.unlock_location(longitude=longitude, latitude=latitude)

    def __eq__(self, other: RebusPost) -> bool:
        return self.id == other.id

    def __hash__(self):
        return self.id.int


class RebusPostTranscoding(Transcoding):
    type = RebusPost
    name = "RebusPost"

    def encode(self, obj: RebusPost) -> Dict[str, Any]:
        return obj.__dict__

    def decode(self, data: Dict[str, Any]) -> RebusPost:
        return RebusPost(**data)


@strawberry.type
class RebusPostQuestionTeamAnswer:
    id: strawberry.ID
    answer: str
    score: float

    def __init__(
        self,
        answer: Optional[str] = None,
        score: Optional[float] = None,
        id: Optional[strawberry.ID] = None,
    ) -> None:
        if id is None:
            self.id = uuid4()
        else:
            self.id = id
        self.answer = answer
        if score is None:
            self.score = 0
        else:
            self.score = score

    def __eq__(self, other: RebusPostQuestion) -> bool:
        return self.id == other.id

    def __hash__(self):
        return self.id.int


class RebusPostQuestionTeamAnswerTranscoding(Transcoding):
    type = RebusPostQuestionTeamAnswer
    name = "RebusPostQuestionTeamAnswer"

    def encode(self, obj: RebusPostQuestionTeamAnswer) -> Dict[str, Any]:
        return obj.__dict__
        return d

    def decode(self, data: Dict[str, Any]) -> RebusPostQuestionTeamAnswer:
        return RebusPostQuestionTeamAnswer(**data)


@strawberry.type
class RebusPostQuestion:
    id: strawberry.ID
    question: str
    media: List[MediaAsset]
    answer: Optional[str]

    def __init__(
        self,
        question: str,
        media: Optional[List[MediaAsset]] = None,
        answer: Optional[str] = None,
        team_answers: Optional[Dict[Team, RebusPostQuestionTeamAnswer]] = None,
        id: Optional[strawberry.ID] = None,
    ) -> None:
        if id is None:
            self.id = uuid4()
        else:
            self.id = id
        self.question = question
        if media is None:
            self.media = []
        else:
            self.media = media
        self.answer = answer
        if team_answers is None:
            self.team_answers = {}
        else:
            self.team_answers = team_answers

    def __eq__(self, other: RebusPostQuestion) -> bool:
        return self.id == other.id

    def __hash__(self):
        return self.id.int


class RebusPostQuestionTranscoding(Transcoding):
    type = RebusPostQuestion
    name = "RebusPostQuestion"

    def encode(self, obj: RebusPostQuestion) -> Dict[str, Any]:
        return obj.__dict__
        return d

    def decode(self, data: Dict[str, Any]) -> RebusPostQuestion:
        return RebusPostQuestion(**data)


@strawberry.type
class Team:
    id: strawberry.ID
    name: str
    members: List[TeamMember]
    team_number: int
    unlocked_posts: List[RebusPostQuestion] = None

    def __init__(
        self,
        name: str,
        id: Optional[strawberry.ID] = None,
        members: Optional[List[TeamMember]] = [],
        team_number: Optional[int] = 0,
        unlocked_posts: Optional[List[RebusPostQuestion]] = [],
    ) -> None:
        if id is None:
            self.id = uuid4()
        else:
            self.id = id
        self.name = name
        self.members = members

    def add_team_member(self, member: TeamMember):
        self.members.append(member)

    def remove_team_member(self, member: TeamMember):
        self.members.remove(member)

    def __eq__(self, other: Team) -> bool:
        return self.id == other.id

    def __hash__(self):
        return self.id.int


class TeamTranscoding(Transcoding):
    type = Team
    name = "Team"

    def encode(self, obj: Team) -> Dict[str, Any]:
        return obj.__dict__

    def decode(self, data: Dict[str, Any]) -> Team:
        return Team(**data)


@strawberry.type
class TeamMember:
    id: strawberry.ID
    name: str
    email: Optional[str] = None

    def __init__(
        self,
        name: str,
        email: Optional[str] = None,
        id: Optional[strawberry.ID] = None,
    ) -> None:
        if id is None:
            self.id = uuid4()
        else:
            self.id = id
        self.name = name
        self.email = email

    def update_name(self, name: str):
        self.name = name

    def __eq__(self, other: TeamMember) -> bool:
        return self.id == other.id

    def __hash__(self):
        return self.id.int


class TeamMemberTranscoding(Transcoding):
    type = TeamMember
    name = "TeamMember"

    def encode(self, obj: TeamMember) -> Dict[str, Any]:
        return obj.__dict__

    def decode(self, data: Dict[str, Any]) -> Team:
        return TeamMember(**data)
