<!--
SPDX-FileCopyrightText: 2023 Håvard Moen <post@haavard.name>

SPDX-License-Identifier: GPL-3.0-only
-->

# Rebus Race

[![status-badge](https://woodpecker.haavard.name/api/badges/umglurf/rebus-race/status.svg)](https://woodpecker.haavard.name/umglurf/rebus-race)

A web application for running rebus races. Some of the features are:
* Each rebus has as multiple rebus posts
* The rebus post can have a unlock code for printing out the post and hanging up
  or a geographic location where the teams must locate the post and verify the
  correct location using GPS on their phone
* Multiple questions per rebus post with support for audio, images and videos
  for each question
* Multiple teams with team members
* Log in for both admin and users using openid connect

## Running the application

It is recommended to build a container image to run the application and setup
nginx or similar in front for ssl termination.

The following environment variables are required:
* `SESSION_KEY` - a random string

You also need to choose a persistence module. To use sqlite you can set
* `PERSISTENCE_MODULE=eventsourcing.sqlite`
* `SQLITE_DBNAME=path_to_sqlite_database_file`

To use postgresql see [eventsourcing documentation](https://eventsourcing.readthedocs.io/en/stable/topics/persistence.html#postgresql-module)

For other options, see [eventsourcing documentation](https://eventsourcing.readthedocs.io/en/stable/topics/persistence.html#other-persistence-modules)

### Authentication

If environment variables for Authentication is not set, the app runs without any
Authentication. This is not recommended in production.

You will need to create an oauth registration in your chosen provider, for
instance
[Azure AD](https://learn.microsoft.com/en-us/azure/active-directory/fundamentals/auth-oidc),
[Google](https://developers.google.com/identity/openid-connect/openid-connect),
etc.
Set the reply URL to `https://URL/auth` where `URL` is the URL you have choosen
to deploy the application to. To also test Authentication locally, you also need
to add `http://localhost:8000/auth` to the list of reply URLs.

The following environment variables must be set:
* `ADMIN_OAUTH_CLIENT_ID` - client id from your oauth provider registration
* `ADMIN_OAUTH_CLIENT_SECRET` - client secret from your oauth provider registration
* `ADMIN_OAUTH_METADATA_URL` - URL to the openid metadata. Some examples are
  * Azure - `https://login.microsoftonline.com/TENANT_ID/.well-known/openid-configuration`
  * Google - `https://accounts.google.com/.well-known/openid-configuration`
* ADMIN_VALID_EMAILS - json list of admin emails, for example `'["someone@examle.com"]``

For user (team member) authentication, you can use the same openid registration
as for admin, or a seperate one. The reply URL must be set to, or added if using
the same as for admin, `https://URL/teamauth`.
The following variables must be set to turn on
authentication:
* `USER_OAUTH_CLIENT_ID` - client id from your oauth provider registration
* `USER_OAUTH_CLIENT_SECRET` - client secret from your oauth provider registration
* `USER_OAUTH_METADATA_URL` - URL to the openid metadata. Some examples are


## Local development

To develop this application, you need to set up
[pipenv](https://pipenv.pypa.io/en/latest/index.html) and then run
`pipenv install -d` to install all required packages.

You need to set a session key:
```
export SESSION_KEY=some_random_value
```
To keep data between reload, the following environment variables are also
recommended:
```
export PERSISTENCE_MODULE=eventsourcing.sqlite
export SQLITE_DBNAME=dev.db
```

You can now run
```
pipenv run uvicorn --reload app.main:app
```

and then access the app on [http://localhost:3000](http://localhost:3000)
